const SERVICE_DATA = {
  heading: "Our Services",
  description:
    "We craft digital, graphic and dimensional thinking, to create category leading brand experiences that have meaning and add a value for our clients.",
  services_list: [
    {
      label: "Digital Design",
      text: "Some quick example text to build on the card title and make up the bulk of the card's content. Moltin gives you the platform.",
      icon: "pe-7s-arc",
    },
    {
      label: "Unlimited Colors",
      text: "Credibly brand standards compliant users without extensible services. Anibh euismod tincidunt ut laoreet.",
      icon: "pe-7s-display2",
    },
    {
      label: "Strategy Solutions",
      text: "Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean necessary regelialia.",
      icon: "pe-7s-piggy",
    },
    {
      label: "Awesome Support",
      text: "It is a paradisematic country, in which roasted parts of sentences fly into your mouth leave for the far World.",
      icon: "pe-7s-science",
    },
    {
      label: "Truly Multipurpose",
      text: "Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.",
      icon: "pe-7s-news-paper",
    },
    {
      label: "Easy to customize",
      text: "Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia.",
      icon: "pe-7s-plane",
    },
  ],
};

const FEATURES_DATA = {
  title:
    "A digital web design studio creating modern & engaging online experiences",
  text: "Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia.",
  feature_list: [
    "We put a lot of effort in design.",
    "The most important ingredient of successful website.",
    "Sed ut perspiciatis unde omnis iste natus error sit.",
    "Submit Your Orgnization.",
  ],
};

const PRICING_DATA = {
  title: "Our Pricing",
  description:
    "Call to action pricing table is really crucial to your for your business website. Make your bids stand-out with amazing options.",
  pricing_list: [
    {
      title: "Economy",
      price: "$9.90",
      bandwidth: "1GB",
      space: "50MB",
      support: "No",
      domain: 1,
    },
    {
      title: "Delux",
      price: "$19.90",
      bandwidth: "10GB",
      space: "500MB",
      support: "Yes",
      domain: 10,
    },
    {
      title: "Unlimate",
      price: "$29.90",
      bandwidth: "100GB",
      space: "2 GB",
      support: "Yes",
      domain: "Unlimited",
    },
  ],
};

const TEAM_DATA = {
  title: "Behind The People",
  description:
    "It is a long established fact that create category leading brand experiences a reader will be distracted by the readable content of a page when looking at its layout.",
  team_list: [
    {
      profile: "/images/team/img-1.jpg",
      name: "Frank Johnson",
      designation: "CEO",
    },
    {
      profile: "/images/team/img-2.jpg",
      name: "Elaine Stclair",
      designation: "Designer",
    },
    {
      profile: "/images/team/img-3.jpg",
      name: "Wanda Arthur",
      designation: "Developer",
    },
    {
      profile: "/images/team/img-4.jpg",
      name: "Joshua Stemple",
      designation: "Manager",
    },
  ],
};

const TESATIMONIAL_DATA = {
  title: "What they've said",
  description:
    "The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious pulvinar metus molestie sed Semikoli.",
  testimonial_list: [
    {
      profile: "images/testimonials/user-2.jpg",
      description:
        "I feel confident imposing change on myself. It's a lot more fun progressing than looking back. That's why scelerisque pretium dolor, sit amet vehicula erat pelleque need throw curve balls.",
      name: "Ruben Reed - ",
      text: "Charleston",
    },
    {
      profile: "images/testimonials/user-1.jpg",
      description:
        "Our task must be to free ourselves by widening our circle of compassion to embrace all living creatures Integer varius lacus non magna tempor congue natuasre the whole its beauty.",
      name: "Michael P. Howlett - ",
      text: "Worcester",
    },
    {
      profile: "images/testimonials/user-3.jpg",
      description:
        "I've learned that people will forget what you said, people will forget what you did, but people will never aliquam in nunc quis tincidunt forget how you vestibulum egestas them feel.",
      name: "Theresa D. Sinclair - ",
      text: "Lynchburg",
    },
  ],
};

const BLOG_DATA = {
  title: "Blog",
  description:
    "Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean class at a euismod mus ipsum vel ex finibus semper luctus quam.",
  blog_list: [
    {
      image: "images/blog/img-1.jpg",
      text: "UI & UX Design",
      title: "Doing a cross country road trip",
      subtext:
        "She packed her seven versalia, put her initial into the belt and made herself on the way..",
    },
    {
      image: "images/blog/img-2.jpg",
      text: "Digital Marketing",
      title: "New exhibition at our Museum",
      subtext:
        "Pityful a rethoric question ran over her cheek, then she continued her way.",
    },
    {
      image: "images/blog/img-3.jpg",
      text: "Travelling",
      title: "Why are so many people..",
      subtext:
        "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia.",
    },
  ],
};

const MOCK_DATA = {
  SERVICE_DATA,
  FEATURES_DATA,
  PRICING_DATA,
  TEAM_DATA,
  TESATIMONIAL_DATA,
  BLOG_DATA,
};

export default MOCK_DATA;
